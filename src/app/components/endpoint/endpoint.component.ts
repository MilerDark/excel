import { Component, OnInit } from '@angular/core';
import { ExporterService } from 'src/app/services/exporter.service';


export interface PeriodicElement {
  nombre: string;
  position: number;
  edad: number;
  genero: string;
}



const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, nombre: 'Juan', edad: 23, genero: 'Masculino'},
  {position: 2, nombre: 'Matias', edad: 44, genero: 'Masculino'},
  {position: 3, nombre: 'Maria', edad: 21, genero: 'Femenino'},
  {position: 4, nombre: 'Luisa', edad: 24, genero: 'Femenino'},
  {position: 5, nombre: 'Rolando', edad: 41, genero: 'Masculino'},
  {position: 6, nombre: 'Messi', edad: 35, genero: 'Masculino'},
  {position: 7, nombre: 'Alex', edad: 33, genero: 'Masculino'},
  {position: 8, nombre: 'Mateo', edad: 11, genero: 'Masculino'},
  {position: 9, nombre: 'Patricia', edad: 65, genero: 'Femenino'},
  {position: 10, nombre: 'Noelia', edad: 22, genero: 'Femenino'},
];


@Component({
  selector: 'app-endpoint',
  templateUrl: './endpoint.component.html',
  styleUrls: ['./endpoint.component.css']
})
export class EndpointComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;
  clickedRows = new Set<PeriodicElement>();

  constructor(private excelService: ExporterService) { }

  ngOnInit(): void {
  }

  exportAsXLSX(): void{
    this.excelService.exportToExcel(this.dataSource, 'my export')
  }


}
